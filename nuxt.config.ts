// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
	runtimeConfig: {
		public: {
			backendUrl: process.env.BACKEND_URL ?? "http://localhost:4000",
			apiKey: process.env.APIKEY,
			authDomain: process.env.AUTHDOMAIN,
			projectId: process.env.PROJECTID,
			storageBucket: process.env.STORAGEBUCKET,
			messagingSenderId: process.env.MESSAGINGSENDERID,
			appId: process.env.APPID,
		},
	},
	modules: [
		"@nuxtjs/tailwindcss",
		"@nuxtjs/color-mode",
		"@vueuse/nuxt",
		"nuxt-icon",
	],
	ssr: false,
	colorMode: {
		preference: "pastel", // default theme
		dataValue: "theme", // activate data-theme in <html> tag
		classSuffix: "",
	},
});
