import { initializeApp } from 'firebase/app'
import { getAuth } from 'firebase/auth'
import { getFirestore } from 'firebase/firestore'

export default defineNuxtPlugin(({ $config }) => {
  const app = initializeApp($config.public)

  const auth = getAuth(app)
  auth.useDeviceLanguage()

  const firestore = getFirestore(app)

  return {
    provide: {
      firebase: {
        auth,
        firestore,
      },
    },
  }
})
