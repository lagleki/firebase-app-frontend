# Nuxt 3 Firebasephone number frontend 

A very simple app interacting with (https://gitlab.com/lagleki/firebase-app).

Uses phone authentication.

<!--# Try demo

https://firebase-app-frontend.vercel.app/

-->

## Deploying locally

* `cp .env.example .env`
* Fill in all fields from Firebase console firebaseConfig variable
* `yarn`
* `yarn dev`

## Production

Build the application for production:

```bash
yarn build
```

## Database structure

This is how data look like in the DB:

![firebase-1](./assets/images/firebase-1.png)
![firebase-2](./assets/images/firebase-2.png)
